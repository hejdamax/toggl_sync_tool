# Toggl time entries sync


## Usage

copy `.env.example` to `.env`.
change the variables to your information.

This program can send system notifications. Only works on linux and if the *Notifications* env variable is set to True.

If you want to run this tool automatically create a crontab entry. Below is a example crontab entry that runs this tool at half past eight every day.

```
30 20 * * * python3 absolute_path_to_main.py
```

Then just drop the *Toggl_time_entries_<date>.csv* into the base dir folder and it will be synced automatically. 