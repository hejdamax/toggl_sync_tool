import os
import glob
import csv
import requests
import json
import base64
from datetime import datetime, timedelta, date, timezone
from dateutil import tz
import logging
from logging.handlers import RotatingFileHandler
import os
from os import environ as env
from dotenv import load_dotenv

# Load config variables
load_dotenv(os.path.split(__file__)[0] + '/.env')
api_token = env['TOGGL_API_TOKEN']
name = env['NAME']
email = env['EMAIL']
workspace_id = env['TOGGL_WORKSPACE_ID']
base_path = env['BASE_PATH']
notifications = env['NOTIFICATIONS'] == 'True'

token_bytes = f"{api_token}:api_token".encode('ascii')
encoded_token = base64.b64encode(token_bytes).decode('ascii')

# LOGGING

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

errorLog = logging.getLogger("Error log")
errorLog.setLevel(logging.ERROR)
errorLogHandler = RotatingFileHandler(f'{base_path}/logs/error.log', maxBytes=1000000, backupCount=5)
errorLogHandler.setLevel(logging.ERROR)
errorLogHandler.setFormatter(formatter)
errorLog.addHandler(errorLogHandler)

statusLog = logging.getLogger("Status log")
statusLog.setLevel(logging.DEBUG)
statusLogHandler = RotatingFileHandler(f'{base_path}/logs/status.log', maxBytes=1000000, backupCount=5)
statusLogHandler.setLevel(logging.DEBUG)
statusLogHandler.setFormatter(formatter)
statusLog.addHandler(statusLogHandler)

# On linux this sends info to system.
def send_notification(summary, body=None, icon=None):
    cmd = f'notify-send "{summary}" "{body}"'
    if icon:
        cmd += f' -i {icon}'
    os.system(cmd)

def convertDate(date, time):
    datetime_str = f'{date}T{time}+02:00'  # Combine date, time, and timezone offset
    dt = datetime.strptime(datetime_str, '%Y-%m-%dT%H:%M:%S%z')
    return dt.isoformat()

def checkDateTime(dateTimeExisting, date, time):
    dateExisting = datetime.strptime(dateTimeExisting,  '%Y-%m-%dT%H:%M:%S%z').date().isoformat()
    timeExisting = datetime.strptime(dateTimeExisting,  '%Y-%m-%dT%H:%M:%S%z').replace(tzinfo=tz.tzutc()).astimezone(tz.gettz('Europe/Berlin')).time().isoformat()
    return  dateExisting == date and timeExisting == time

def formatTimeEntry(timeEntry):
    client = timeEntry["Client"]
    project = timeEntry["Project"]
    description = timeEntry["Description"]
    time = f'{timeEntry["Start time"]} - {timeEntry["End time"]} {timeEntry["Start date"] if timeEntry["Start date"] == timeEntry["End date"] else timeEntry["Start date"] - timeEntry["End date"]}'
    if client != "":
        return f'Client: {client}, Project: {project}, Description: {description}, Time: {time}'
    return f'Project: {project}, Description: {description if description != "" else "-"}, Time: {time}'

# Find the newest CSV file in the current directory
csv_files = glob.glob(os.path.join(base_path, '*.csv'))
newest_csv = max(csv_files, key=os.path.getctime)

# Open the CSV file and replace the email and name
with open(newest_csv, 'r') as f:
    reader = csv.DictReader(f)
    data = [row for row in reader]

last_month_start = (datetime.now() - timedelta(days=30)).date().isoformat()
last_month_end = (date.today() + timedelta(1)).isoformat()


url = f'https://api.track.toggl.com/api/v9/me/time_entries'
params = {'start_date': last_month_start, 'end_date': last_month_end}
headers = {'Content-Type': 'application/json', 'Authorization': 'Basic ' + encoded_token}
r = requests.get(url, params=params, headers=headers)

if r.status_code != 200:
    print(f'Error getting time entry list: {r.text}')
    existing_entries = []
else:
    existing_entries = r.json()

# Get the list of projects in the workspace
url = f'https://api.track.toggl.com/api/v9/workspaces/{workspace_id}/projects'
headers = {'Content-Type': 'application/json', 'Authorization': 'Basic ' + encoded_token}
r = requests.get(url, headers=headers)

if r.status_code != 200:
    print(f'Error getting project list: {r.text}')
    projects = []
else:
    projects = r.json()

# Send the time entries to Toggl via the API
url = 'https://api.track.toggl.com/api/v9/time_entries'
headers = {'Content-Type': 'application/json', 'Authorization': 'Basic ' + encoded_token}

entries = len(data)
skipped = 0
synced = 0
error = 0
for row in data:
    row['Email'] = email
    row['Name'] = name

    # Find the project ID for this time entry
    project = next((p for p in projects if p['name'] == row['Project']), None)
    if project is None:
        print(f'Error: Project "{row["Project"]}" not found in workspace')
        continue

    existing_entry = next(
        (e for e in existing_entries if e['description'] == row['Description'] and 
        checkDateTime(e['start'], row['Start date'],row['Start time'])  and 
        e['pid'] == project['id'] and 
        checkDateTime(e['stop'], row['End date'], row['End time'])), None)
    if existing_entry is not None:
        skipped += 1
        # print(f'Skipping time entry `{formatTimeEntry(row)}` - already exists')
        continue

    # Send the time entry to Toggl
    payload = {
        'description': row['Description'],
        "start": convertDate(row["Start date"], row["Start time"]),
        'stop': convertDate(row["End date"], row["End time"]),
        'pid': project['id'],
        'billable': (True if row['Billable'].lower() == "yes" else False),
        'tags': row['Tags'].split(','),
        'created_with': 'meeting_sync_jagu',
        'workspace_id': int(workspace_id),
    }
    r = requests.post(url, data=json.dumps(payload), headers=headers)

    if r.status_code != 200:
        print(r.status_code)
        error += 1
        errorLog.error(f'Error sending time entry `{formatTimeEntry(row)}`: {r.text}')
    else:
        synced += 1
statusLog.info(f"Skipped: {skipped}, Synced: {synced}, Error: {error}, Total: {entries}")

# On linux this sends info to system.
if notifications:
    if error == 0:
        if skipped < entries:
                send_notification("Standup sync Completed", f"The sync finished with this statistic:\n Skipped: {skipped}, Synced: {synced}, Error: {error}, Total: {entries}.", base_path + "/icons/emblem-synchronizing-symbolic.svg")
    else:
        send_notification("Standup sync Failed", "Please check error log for errors.", base_path + "/icons/images.jpeg" )    
